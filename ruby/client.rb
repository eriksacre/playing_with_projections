require 'json'
require 'rest-client'

def read_from_uri stream_id
  stream = "https://playing-with-projections.herokuapp.com/stream/#{stream_id}"
#  stream = "https://raw.githubusercontent.com/tcoopman/playing_with_projections_server/master/data/#{stream_id}.json"
  puts "Reading from '#{stream}'"
  RestClient.get stream
end

def read_from_file stream_id
  file_path = "../data/#{stream_id}.json"
  puts "Reading from '#{file_path}'"
  file_content = File.read(file_path)
end

# Helpers
def seconds_between(start_time, end_time)
  Time.parse(end_time) - Time.parse(start_time)
end

# Some projections
def number_of_events(events)
  events.length
end

def event_types(events)
  events.reduce({}) do |acc, event|
    event_type = event['type']
    acc[event_type] = event_type
    acc
  end.values
end

def number_of_event_types(events)
  event_types(events).length
end

def number_of_games_started(events)
  events.reduce(0) do |acc, event|
    event['type'] == 'GameWasStarted' ? acc + 1 : acc
  end
end

def longest_5_games(events)
  events.reduce({}) do |acc, event|
    game_id = event['payload']['game_id']
    acc[game_id] = { 'game_id' => game_id, 'start_time' => event['timestamp'] } if event['type'] == 'GameWasStarted'
    acc[game_id]['duration'] = seconds_between(acc[game_id]['start_time'], event['timestamp']) if event['type'] == 'GameWasFinished'
    acc
  end.values.sort_by do |value|
    -value['duration']
  end.first(5)
end

stream_id = ARGV.first || 0

# json_data = read_from_uri(stream_id)
json_data = read_from_file(stream_id)

events = JSON.parse(json_data)
puts "Number of events: #{number_of_events(events)}"
puts "Event types: #{event_types(events)}"
puts "Number of event types: #{number_of_event_types(events)}"
puts "Number of games started: #{number_of_games_started(events)}"
puts "Longest 5 games: #{longest_5_games(events)}"
